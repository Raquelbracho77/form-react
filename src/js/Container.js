import React, {Fragment, useState, useEffect} from 'react';
import Header from './Header';
import Section from './Section';
import List from './List';
import Form from './formRecords';
import '../css/records.scss'

const recordsData = [
	{
		recordName : 'React Rave',
		artistName: 'The developers',
		description: 'Lorem Ipsum',
	},
	{
		recordName : 'Building an App',
		artistName: 'The Components',
		description: 'Sounds of the future.',
	},
];


const Container = () => {
	const [recordsData, setRecords] = useState([]);
	const [liveText, setLiveText] = useState('');

	

	const onSubmitHandler = entry => {
		setRecords(
			[...recordsData, entry].sort(( a,b ) =>{
				if( a.recordName < b.recordName){
					return -1;
				}
				if(a.recordName > b.recordName){
					return 1;
				}
				return 0;
			})
		);
		setLiveText(`${entry.recordName}succesfully added.`);
	};

return(
<Fragment>
<Header />
	<main>
		<Section headingText="Add a new favourite">
			<Form onSubmit={onSubmitHandler} />
		</Section>
		<Section headingText="Records">
			<List recordsData={recordsData}/>
		</Section>
	</main>
	<div 
		className="visually-hidden" 
		aria-live="polite" 
		aria-atomic="true"
	>
		{liveText}
	</div>
</Fragment>
)
};

export default Container;