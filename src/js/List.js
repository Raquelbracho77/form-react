import React from 'react';

const recordsData = [
	{
		recordName : 'React Rave',
		artistName: 'The developers',
		description: 'Lorem Ipsum',
	},
	{
		recordName : 'Building an App',
		artistName: 'The Components',
		description: 'Sounds of the future.',
	},
];

const List = ( {recordsData} ) => (

	<ul>
		{recordsData.map(( {recordName, artistName, description}) => (
			<li key={recordName}>
				<h3>{recordName}</h3>
				<span>{artistName}</span>
				<p>{description}</p>
			</li>
		))}
	</ul>
);

export default List